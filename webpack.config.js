var webpack = require("webpack");
var path = require("path");

module.exports = {
	devServer: {
		disableHostCheck: true,
		clientLogLevel: 'warning'   //https://webpack.js.org/configuration/dev-server/#devserverclientloglevel
	},
	stats: {
		warningsFilter: [
			(warning) => true
		]
	},
	module: {
		rules: [
			// Apply loader
			{
				test: /\.scss$/,
				use: [
					{
						loader: 'sass-resources-loader',
						options: {
							// Provide path to the file with resources
							resources: [
								'./src/assets/sass/var/variables.scss',
								'./src/assets/sass/var/mixins.scss',
								'./src/assets/sass/vendor/bootstrap-resources.scss',
								'./src/assets/sass/var/bootstrap-overrides.scss',
								'./src/assets/sass/typography.scss',
							],
						},
					}
				],
			},
		],
	}
};

