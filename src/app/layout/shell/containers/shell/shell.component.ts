import { Component } from '@angular/core';

@Component({
  selector: 'app-shell',
  styleUrls: ['./shell.component.scss'],
  template: `
    <div class="app-shell" [class.is-mobile]="isMobile()">
        <!-- header -->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link" routerLink="/" routerLinkActive="is-active">Home</a>
                    <a class="nav-link" routerLink="/kanban" routerLinkActive="is-active">Kanban Board</a>
                </div>
            </div>
        </div>

        <div class="content-main">
            <router-outlet></router-outlet>
        </div>
        <!-- footer -->
    </div>
    `,
})
export class ShellComponent {
  isMobile() {
    return !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  }
}
