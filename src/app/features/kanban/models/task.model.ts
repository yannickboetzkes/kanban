import { v4 as uuidv4 } from 'uuid';

export const statusDict = {
  backlog: {
    id: 'backlog',
    name: 'Backlog',
  },
  inProgress: {
    id: 'inProgress',
    name: 'In Progress',
  },
  done: {
    id: 'done',
    name: 'Done',
  },
};

export interface IStatus {
  id: string;
  name: string;
}

export class Task {
  public id: string;
  public init: boolean;

  constructor(
    public title: string,
    public description: string,
    public status: IStatus,
  ) {
    this.id = uuidv4();
    this.init = true;
  }
}
