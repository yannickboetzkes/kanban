import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IStatus, statusDict, Task } from '../../models/task.model';

@Component({
  selector: 'board-task',
  styleUrls: ['./board-task.component.scss'],
  template: `
    <div class="board-task" *ngIf="tempTask">
        <div class="bt__task">
            <div class="task__inner">
                <div class="task__title">
                    <h5>Title</h5>
                    <p *ngIf="!edit; else title">{{ tempTask.title }}</p>
                    <ng-template #title>
                        <input type="text" placeholder=" " [(ngModel)]="tempTask.title">
                    </ng-template>
                </div>
                <div class="task__info">
                    <h5>Description</h5>
                    <p *ngIf="!edit; else description">{{ tempTask.description }}</p>
                    <ng-template #description>
                        <textarea type="text" placeholder=" " [(ngModel)]="tempTask.description"></textarea>
                    </ng-template>
                </div>
                <div class="task__actions ta mt-3">
                    <div class="mb-3">
                        <span *ngIf="edit" class="mr-2">Status</span>
                        <select *ngIf="edit" [ngModel]="tempTask.status.id" (ngModelChange)="onStatusChange($event)">
                            <option *ngFor="let status of statuses" [value]="status.id">{{ status.name }}</option>
                        </select>
                    </div>
                    <div>
                        <button class="btn btn-primary mr-2" *ngIf="edit" (click)="onClickSave()">Save</button>
                        <button class="btn btn-secondary mr-2" *ngIf="!edit" (click)="onClickEdit()">Edit</button>
                        <button class="btn btn-warning mr-2" *ngIf="edit" (click)="onClickDelete()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,
})
export class BoardTaskComponent {
  @Input() set task(value: Task) {
    this.tempTask = { ...value };
    if (value.init) {
      this.edit = true;
    }
  }

  @Output() submitted = new EventEmitter<Task>();
  @Output() deleted = new EventEmitter<Task>();

  tempTask: Task;
  edit = false;
  statuses: IStatus[] = Object.values(statusDict);

  onClickSave() {
        // todo add form validation
    this.edit = false;
    this.tempTask.init = false;
    this.submitted.emit(this.tempTask);
  }

  onStatusChange(newStatus) {
    this.tempTask.status = statusDict[newStatus];
  }

  onClickEdit() {
    this.edit = true;
  }

  onClickDelete() {
    this.deleted.emit(this.tempTask);
  }
}
