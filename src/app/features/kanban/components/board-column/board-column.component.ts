import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IStatus, Task } from '../../models/task.model';

@Component({
  selector: 'board-column',
  styleUrls: ['./board-column.component.scss'],
  template: `
    <div class="board-column" [ngClass]="column.id">
        <h3 class="headline p-3">{{ column.name }}</h3>
        <div class="bc__task-list">
            <board-task *ngFor="let task of tasks"
                        [task]="task"
                        (submitted)="onSubmitted($event)"
                        (deleted)="onDeleted($event)"
            ></board-task>
        </div>
    </div>
    `,
})
export class BoardColumnComponent {
  @Input() column: IStatus;
  @Input() tasks: Task[];
  @Output() submitted = new EventEmitter();
  @Output() deleted = new EventEmitter();

  onSubmitted($event: Task) {
    this.submitted.emit($event);
  }

  onDeleted($event: Task) {
    this.deleted.emit($event);
  }
}
