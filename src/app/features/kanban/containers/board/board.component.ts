import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IStatus, statusDict, Task } from '../../models/task.model';
import { TaskActions } from '../../store/actions/task.actions';
import { TaskSelectors } from '../../store/selectors/task.selectors';


@Component({
  selector: 'kanban-board',
  styleUrls: ['./board.component.scss'],
  template: `
    <div class="kanban-board">
        <div class="container">
            <div class="headline">
                <div class="row">
                    <div class="col-12">
                        <h1>Kanban Board</h1>
                    </div>
                </div>
            </div>
            <div class="actions">
                <div class="row">
                    <div class="col-12">
                        <button class="add-task btn btn-primary mb-3" (click)="onAddTask()">Add task</button>
                    </div>
                </div>
            </div>
            <div class="board">
                <div class="row">
                    <div class="col-12">
                        <div class="slider">
                            <board-column
                                class="slider-item"
                                *ngFor="let column of board"
                                [column]="column"
                                [tasks]="getTasks(column.id) | async"
                                (submitted)="onUpdateTask($event)"
                                (deleted)="onDeleteTask($event)"
                        >
                        </board-column>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,
})
export class BoardComponent {
  board: IStatus[] = Object.values(statusDict);

  constructor(
    private store: Store<{}>,
  ) {}

  getTasks(statusId: string) {
    return this.store.select(TaskSelectors.selectTasksByStatus(statusId));
  }

  onAddTask() {
    const task = new Task(
            null,
            null,
            statusDict.backlog,
        );
    this.store.dispatch(TaskActions.createTask({ task }));
  }

  onUpdateTask(task: Task) {
    this.store.dispatch(TaskActions.updateTask({ task: { id: task.id, changes: task } }));
  }

  onDeleteTask($event: Task) {
    this.store.dispatch(TaskActions.deleteTask({ id: $event.id }));
  }
}
