import { TestBed } from '@angular/core/testing';
import { Dictionary } from '@ngrx/entity';
import { Store, StoreModule } from '@ngrx/store';
import { coreReducers } from '../../../../core/store';
import { statusDict, Task } from '../../models/task.model';
import { TaskActions } from '../actions/task.actions';
import { kanbanReducers, kanbanStateKey } from '../reducers';
import { TaskSelectors } from './task.selectors';


describe('TaskSelectors', () => {
  // setup
  let store: Store<{}>;
  const tasks = [
    new Task('A task', 'A task description', statusDict.backlog),
    new Task('Test task', 'A test description', statusDict.done),
  ];
  const entities: Dictionary<Task> = {
    [tasks[0].id]: tasks[0],
    [tasks[1].id]: tasks[1],
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...coreReducers,
          [kanbanStateKey]: kanbanReducers,
        }),
      ],
    });

    store = TestBed.inject(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  // tests
  describe('selectEntities', () => {
    it('should return tasks as entities', () => {
      let result;
      store.select(TaskSelectors.selectEntities).subscribe(value => result = value);

      store.dispatch(TaskActions.createTask({ task: tasks[0] }));
      store.dispatch(TaskActions.createTask({ task: tasks[1] }));
      expect(result).toEqual(entities);
    });
  });

  describe('selectTasksByStatus', () => {
    it('should return tasks as array', () => {
      let result;
      store.select(TaskSelectors.selectTasksByStatus(statusDict.done.id))
          .subscribe(value => result = value);
      store.dispatch(TaskActions.createTask({ task: tasks[0] }));
      const updatedTask = {
        id: tasks[0].id,
        changes: { ...tasks[0], status: statusDict.done },
      };
      store.dispatch(TaskActions.updateTask({ task: updatedTask }));
      expect(result).toEqual([{ ...tasks[0], status: statusDict.done }]);
    });
  });
});
