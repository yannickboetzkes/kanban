import { createSelector } from '@ngrx/store';
import { selectKanbanState } from '../reducers';
import { adapter, taskFeatureKey } from '../reducers/task.reducer';

const taskEntitySelectors = adapter.getSelectors();

export namespace TaskSelectors {

    export const taskState = createSelector(
      selectKanbanState,
      state => state[taskFeatureKey],
    );

    export const selectEntities = createSelector(
      taskState,
      taskEntitySelectors.selectEntities,
    );

    export const selectTasksByStatus = (statusId: string) => createSelector(
      selectEntities,
      tasks => Object.values(tasks).filter(task => task.status.id === statusId),
    );

}
