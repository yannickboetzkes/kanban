import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import { TaskEntityState, taskFeatureKey, taskReducer } from './task.reducer';

export const kanbanStateKey = 'kanbanState';

export interface KanbanState {
  [taskFeatureKey]: TaskEntityState;
}

export function kanbanReducers(state: KanbanState | undefined, action: Action) {
  return combineReducers({
    [taskFeatureKey]: taskReducer,
  })(state, action);
}

export const selectKanbanState = createFeatureSelector<KanbanState>(kanbanStateKey)
