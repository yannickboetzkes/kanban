import { statusDict, Task } from '../../models/task.model';
import { TaskActions } from '../actions/task.actions';
import { initialState, taskReducer } from './task.reducer';


describe('TaskReducer', () => {
  const task = new Task(
            'A task',
            'A task description',
            statusDict.backlog,
        );

  describe('Default', () => {
    it('should return the default state', () => {
      const action = { type: 'NOOP' } as any;
      const result = taskReducer(undefined, action);
      expect(result).toBe(initialState);
    });
  });

  describe('createTask', () => {
    it('should create a task', () => {
      const action = TaskActions.createTask({ task });
      const result = taskReducer(initialState, action);

      expect(result).toEqual({
        ...initialState,
        entities: {
          [task.id]: task,
        },
        ids: [task.id],
      });
    });
  });

  describe('updateTask', () => {
    it('should update a task', () => {
      const baseAction = TaskActions.createTask({ task });
      const baseState = taskReducer(initialState, baseAction);
      const updatedTask: Task = { ...task, status: statusDict.done };
      const action = TaskActions.updateTask({ task: { id: updatedTask.id, changes: updatedTask } });
      const result = taskReducer(baseState, action);

      expect(result).toEqual({
        ...initialState,
        entities: {
          ...initialState.entities,
          [updatedTask.id]: updatedTask,
        },
        ids: [
          ...initialState.ids,
          updatedTask.id,
        ],
      });
    });
  });

  describe('deleteTask', () => {
    it('should delete a task', () => {
      const baseAction = TaskActions.createTask({ task });
      const baseState = taskReducer(initialState, baseAction);
      const action = TaskActions.deleteTask({ id: task.id });
      const result = taskReducer(baseState, action);
      const expectState = { ...baseState };
      delete expectState.entities[task.id];
      expectState.ids = (<string[]>expectState.ids).filter(val => val !== task.id);

      expect(result).toEqual(expectState);
    });
  });
});

