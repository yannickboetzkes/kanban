import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import { Task } from '../../models/task.model';
import { TaskActions } from '../actions/task.actions';

export const taskFeatureKey = 'Task';

export interface TaskEntityState extends EntityState<Task> {
  entityName: string;
  loaded: boolean;
  loading: boolean;
}

export const adapter = createEntityAdapter<Task>({});

export const initialState: TaskEntityState = adapter.getInitialState({
  entityName: taskFeatureKey,
  loaded: true,
  loading: false,
});


const reducer = createReducer(
  initialState,

  on(TaskActions.createTask, (state, action) => {
    return adapter.addOne(action.task, state);
  }),

  on(TaskActions.updateTask, (state, action) =>
      adapter.updateOne(action.task, state),
  ),

  on(TaskActions.deleteTask, (state, action) =>
        adapter.removeOne(action.id, state),
    ),

);


export function taskReducer(state: TaskEntityState | undefined, action: Action) {
  return reducer(state, action);
}
