import { Update } from '@ngrx/entity';
import { createAction, props } from '@ngrx/store';
import { Task } from '../../models/task.model';

export namespace TaskActions {

    export const createTask = createAction(
      '[Task] Create Task',
      props<{task: Task}>(),
    );

    export const updateTask = createAction(
      '[Task] Update Task',
      props<{task: Update<Task>}>(),
    );

    export const deleteTask = createAction(
      '[Task] Delete Task',
      props<{id: string}>(),
    );

}
