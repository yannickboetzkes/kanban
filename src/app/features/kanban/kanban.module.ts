import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { BoardTaskComponent } from './components/board-task/board-task.component';
import { BoardComponent } from './containers/board/board.component';
import { kanbanReducers, kanbanStateKey } from './store/reducers';
import { BoardColumnComponent } from './components/board-column/board-column.component';


const routes: Routes = [
  {
    path: '',
    component: BoardComponent,
  },
];

@NgModule({
  declarations: [
    BoardComponent,
    BoardTaskComponent,
    BoardColumnComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature(kanbanStateKey, kanbanReducers),
    FormsModule,
  ],
})
export class KanbanModule { }
