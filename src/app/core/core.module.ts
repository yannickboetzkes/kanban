import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { stateSetter } from '../hmr.module';
import { extModules } from './configurations/ngrx-dev-tools';

import { ROOT_REDUCER } from './store';
import { coreEffects } from './store/effects';
import { appReducers } from './store/reducers';
import { CustomRouteSerializer } from './store/reducers/router.reducer';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forRoot(ROOT_REDUCER, {
      metaReducers: [stateSetter],
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      },
    }),
    EffectsModule.forRoot(coreEffects),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'routerState',
      serializer: CustomRouteSerializer,
    }),
    extModules,
  ],
})
export class CoreModule {}
