import { ActivatedRouteSnapshot, Params, RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';

export interface RouterState {
  url: string;
  queryParams: Params;
  params: Params;
  data: any;
}

export class CustomRouteSerializer implements RouterStateSerializer<RouterState> {
    // this function is called automatically on every route change
  serialize(routerState: RouterStateSnapshot): RouterState {
    const url = decodeURI(routerState.url);

        // todo also decode queryParams and params
    const { queryParams } = routerState.root;
    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }
    const { params, data } = state;

    return { url, queryParams, params, data };
  }
}

