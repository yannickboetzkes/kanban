import { environment } from '../../../environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export const extModules = [];


if (environment.ngrxDevTools) {
  extModules.push(StoreDevtoolsModule.instrument({
    maxAge: 25,
  }));
}
