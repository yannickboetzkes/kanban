import { InjectionToken } from '@angular/core';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { appReducers, AppState, appStateKey } from './reducers';


export interface CoreState {
  [appStateKey]: AppState;
}

export const coreReducers: ActionReducerMap<CoreState> = {
  appState: appReducers,
};

export const ROOT_REDUCER = new InjectionToken('Root Reducer', {
  factory: () => coreReducers,
});
