import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { RouterNavigatedAction } from '@ngrx/router-store';
import { tap } from 'rxjs/operators';
import { RouterActions } from '../actions/router.actions';



/**
 * side effects can be triggered on any router navigation (e.g. http calls on navigating)
 * navigate$ observable will be triggered on any route change
 * fromRouter actions will be used to navigate instead of default router
 */
@Injectable()
export class RouterEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location,
  ) {}


  @Effect({ dispatch: false }) navigate$ = this.actions$
        .pipe(
          ofType(RouterActions.go),
          tap(({ path, query, extras }) => {
            this.router.navigate(path, { queryParams: query, ...extras });
          }),
        );

  @Effect({ dispatch: false }) navigationSuccess$ = this.actions$
        .pipe(
          ofType('@ngrx/router-store/navigated'),
          tap((action: RouterNavigatedAction) => {
                // this.routerFacade.onPageLoaded$(action.payload);
          }),
        );

  @Effect({ dispatch: false }) navigateBack$ = this.actions$
        .pipe(
          ofType(RouterActions.back),
          tap(() => this.location.back()),
        );

  @Effect({ dispatch: false }) navigateForward$ = this.actions$
        .pipe(
          ofType(RouterActions.forward),
          tap(() => this.location.forward()),
        );
}
