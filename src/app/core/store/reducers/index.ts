
import { Action, combineReducers, MetaReducer } from '@ngrx/store';
import { environment } from '../../../../environments/environment';
import { RouterState } from '../../configurations/custom-route-serializer';
import { routerFeatureKey, routerReducer, RouterReducerState } from './router.reducer';

export const appStateKey = 'appState';

export interface AppState {
  [routerFeatureKey]: RouterReducerState<RouterState | any>;
}

export function appReducers(state: AppState | undefined, action: Action) {
  return combineReducers({
    [routerFeatureKey]: routerReducer,
  })(state, action);
}
