import { Params } from '@angular/router';

export { routerReducer, RouterReducerState } from '@ngrx/router-store';

export const routerFeatureKey = 'router';

export interface RouterState {
  url: string;
  queryParams: Params;
  params: Params;
}


export { CustomRouteSerializer } from '../../configurations/custom-route-serializer';

